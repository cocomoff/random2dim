# -*- coding: utf-8 -*-

import uuid
import argparse
import numpy as np
# import networkx as nx
from itertools import product
import matplotlib.pyplot as plt


class Vehicle(object):
    def __init__(self, mn, mx):
        self.uid = str(uuid.uuid4())
        self.v = 0
        self.passenger_ids = []
        self.path = []
        self.capacity = np.random.choice([2, 4, 6], size=1)[0]
        self.x = np.random.uniform(high=mx)
        self.y = np.random.uniform(low=mx)

    def p(self):
        return np.array(self.x, self.y)

    def __str__(self):
        return "{}({}/{};x={:2.2f},y={:2.2f})".format(
        str(self.uid)[:2], self.v, self.capacity, self.x, self.y)


class Passenger(object):
    def __init__(self, mn, mx):
        self.uid = str(uuid.uuid4())
        self.ox = np.random.uniform(high=mx)
        self.oy = np.random.uniform(high=mx)
        self.dx = np.random.uniform(high=mx)
        self.dy = np.random.uniform(high=mx)

    def o(self):
        return np.array([self.ox, self.oy])

    def d(self):
        return np.array([self.dx, self.dy])

    def __str__(self):
        return "{}({:2.2f},{:2.2f} -> {:2.2f},{:2.2f})".format(
        str(self.uid)[:2], self.ox, self.oy, self.dx, self.dy)


class Problem(object):
    def __init__(self, args):
        self.n = args.n
        self.m = args.m
        self.locmin = 0.0
        self.locmax = 10.0
        _range = range(int(self.locmin), int(self.locmax))
        self.grid = product(_range, _range)

        self.vehicles = []
        for _ in range(self.n):
            v = Vehicle(self.locmin, self.locmax)
            # print(v)
            self.vehicles.append(v)

        self.passengers = []
        for _ in range(self.m):
            p = Passenger(self.locmin, self.locmax)
            # print(p)
            self.passengers.append(p)

        route = self.greedy()

        # plot
        self.plot()

    def greedy(self):
        self.A = set({})
        for i in range(self.m):
            pi = self.passengers[i]
            if pi not in self.A:
                for j in range(self.n):
                    vj = self.vehicles[j]
                    print("   {}".format(vj))
                print(pi)
        pass


    def plot(self):
        fig = plt.figure(figsize=(5, 5))
        ax = fig.gca()
        for v in self.vehicles:
            ax.plot(v.x, v.y, 'ro', ms=10)
        for p in self.passengers:
            ax.plot(p.ox, p.oy, 'bs', ms=10, alpha=0.5)
            ax.plot(p.dx, p.dy, 'gs', ms=10, alpha=0.5)
            ax.plot([p.ox, p.dx], [p.oy, p.dy], 'k--')
        ax.axis("off")
        # ax.margins(0, 0, 1, 1)
        plt.savefig("f_output/plot.png", dpi=240)
        plt.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", help='# of vehicles', type=int, action="store", default=5)
    parser.add_argument("-m", help='# of passengers', type=int, action="store", default=3)
    parser.parse_args()
    args = parser.parse_args()
    # print(args)

    problem = Problem(args)
